package ru.design.patterns.bmw;

import ru.design.patterns.cartype.Minivan;
import ru.design.patterns.enums.Color;

import java.util.List;

/**
 * TeslaMinivan.
 *
 * @author Tatyana_Dolnikova
 */
public class BMWMinivan extends Minivan {

    public BMWMinivan(List<Color> colors, Integer speed) {
        super(colors, speed);
    }
}
