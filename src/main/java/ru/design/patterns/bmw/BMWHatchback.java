package ru.design.patterns.bmw;

import ru.design.patterns.cartype.Hatchback;
import ru.design.patterns.enums.Color;

import java.util.List;

/**
 * TeslaHatchback.
 *
 * @author Tatyana_Dolnikova
 */
public class BMWHatchback extends Hatchback {

    public BMWHatchback(List<Color> colors, Double speed) {
        super(colors, speed);
    }
}
