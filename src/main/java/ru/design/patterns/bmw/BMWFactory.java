package ru.design.patterns.bmw;

import static java.util.Arrays.asList;

import lombok.Setter;
import ru.design.patterns.cartype.Car;
import ru.design.patterns.cartype.Hatchback;
import ru.design.patterns.cartype.Minivan;
import ru.design.patterns.cartype.Sedan;
import ru.design.patterns.enums.Color;
import ru.design.patterns.factory.CarFactory;
import ru.design.patterns.kia.KiaHatchback;
import ru.design.patterns.kia.KiaMinivan;
import ru.design.patterns.kia.KiaSedan;

import java.util.Collections;
import java.util.List;

/**
 * TeslaFactory.
 *
 * @author Tatyana_Dolnikova
 */
@Setter
public class BMWFactory implements CarFactory {

    private final Color defaultColor = Color.WHITE;
    private List<Color> availableColors = asList(Color.BLACK, Color.GREEN);
    private Integer sedanSpeed = 260;
    private Integer hatchbackSpeed = 280;
    private Integer minivanSpeed = 200;
    private Double price = 50000.0;
    private Double rate = 1.7;
    private Double discount = 0.02;
    private Double tax = 200.0;

    @Override
    public Sedan createSedan() {
        return new KiaSedan(asList(defaultColor), sedanSpeed);
    }

    @Override
    public Hatchback createHatchback() {
        return new KiaHatchback(asList(defaultColor), hatchbackSpeed);
    }

    @Override
    public Minivan createMinivan() {
        return new KiaMinivan(asList(defaultColor), minivanSpeed);
    }

    /**
     * Покрасить авто в один цвет.
     */
    @Override
    public Car paintCar(Car car, Color color) {
        List<Color> colors = car.getColors();
        if (colors == null) {
            colors = Collections.singletonList(defaultColor);
        }
        if (availableColors.contains(color) && !colors.contains(color)) {
            colors = Collections.singletonList(color);
        }
        car.setColors(colors);
        return car;
    }

    /**
     * Получить полную стоимость авто на основании цены, коэффициента за скорость, скидки и налога.
     */
    @Override
    public Double getPrice(Car car) {
        return price + car.getSpeed() * rate - price * discount + tax;
    }

}
