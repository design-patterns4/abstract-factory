package ru.design.patterns.bmw;

import ru.design.patterns.cartype.Sedan;
import ru.design.patterns.enums.Color;

import java.util.List;

/**
 * TeslaSedan.
 *
 * @author Tatyana_Dolnikova
 */
public class BMWSedan extends Sedan {

    public BMWSedan(List<Color> colors) {
        super(colors);
    }
}
