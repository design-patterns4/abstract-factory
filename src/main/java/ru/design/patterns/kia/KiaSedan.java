package ru.design.patterns.kia;

import ru.design.patterns.cartype.Sedan;
import ru.design.patterns.enums.Color;

import java.util.List;

/**
 * KiaSedan.
 *
 * @author Tatyana_Dolnikova
 */
public class KiaSedan extends Sedan {

    public KiaSedan(List<Color> colors, Integer speed) {
        super(colors, speed);
    }
}
