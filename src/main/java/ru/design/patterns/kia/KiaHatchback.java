package ru.design.patterns.kia;

import ru.design.patterns.cartype.Hatchback;
import ru.design.patterns.enums.Color;

import java.util.List;

/**
 * KiaHatchback.
 *
 * @author Tatyana_Dolnikova
 */
public class KiaHatchback extends Hatchback {

    public KiaHatchback(List<Color> colors, Integer speed) {
        super(colors, speed);
    }
}
