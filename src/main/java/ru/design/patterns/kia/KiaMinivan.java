package ru.design.patterns.kia;

import ru.design.patterns.cartype.Minivan;
import ru.design.patterns.enums.Color;

import java.util.List;

/**
 * KiaMinivan.
 *
 * @author Tatyana_Dolnikova
 */
public class KiaMinivan extends Minivan {

    public KiaMinivan(List<Color> colors, Integer speed) {
        super(colors, speed);
    }
}
