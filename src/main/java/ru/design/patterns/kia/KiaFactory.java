package ru.design.patterns.kia;

import static java.util.Arrays.asList;

import lombok.Setter;
import ru.design.patterns.cartype.Car;
import ru.design.patterns.cartype.Hatchback;
import ru.design.patterns.cartype.Minivan;
import ru.design.patterns.cartype.Sedan;
import ru.design.patterns.enums.Color;
import ru.design.patterns.factory.CarFactory;

import java.util.ArrayList;
import java.util.List;

/**
 * TeslaFactory.
 *
 * @author Tatyana_Dolnikova
 */
@Setter
public class KiaFactory implements CarFactory {

    private final Color defaultColor = Color.YELLOW;
    private List<Color> availableColors = asList(Color.RED, Color.YELLOW, Color.BLUE);
    private Integer sedanSpeed = 220;
    private Integer hatchbackSpeed = 320;
    private Integer minivanSpeed = 180;
    private Double price = 20000.0;
    private Double rate = 1.3;
    private Double discount = 0.05;

    @Override
    public Sedan createSedan() {
        return new KiaSedan(asList(defaultColor), sedanSpeed);
    }

    @Override
    public Hatchback createHatchback() {
        return new KiaHatchback(asList(defaultColor), hatchbackSpeed);
    }

    @Override
    public Minivan createMinivan() {
        return new KiaMinivan(asList(defaultColor), minivanSpeed);
    }

    /**
     * Покрасить авто в несколько цветов.
     */
    @Override
    public Car paintCar(Car car, Color color) {
        List<Color> colors = car.getColors();
        if (colors == null) {
            colors = new ArrayList<>();
            colors.add(defaultColor);
        }
        if (availableColors.contains(color) && !colors.contains(color)) {
            colors.add(color);
        }
        car.setColors(colors);
        return car;
    }

    /**
     * Получить полную стоимость авто на основании цены, коэффициента за скорость и налога.
     */
    @Override
    public Double getPrice(Car car) {
        return price + car.getSpeed() * rate - price * discount;
    }

}
