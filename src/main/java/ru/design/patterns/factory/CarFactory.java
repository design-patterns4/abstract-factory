package ru.design.patterns.factory;

import ru.design.patterns.cartype.Car;
import ru.design.patterns.cartype.Hatchback;
import ru.design.patterns.cartype.Minivan;
import ru.design.patterns.cartype.Sedan;
import ru.design.patterns.enums.Color;

/**
 * CarFactory.
 *
 * @author Tatyana_Dolnikova
 */
public interface CarFactory {

    Sedan createSedan();

    Hatchback createHatchback();

    Minivan createMinivan();

    Car paintCar(Car car, Color color);

    Double getPrice(Car car);

}
