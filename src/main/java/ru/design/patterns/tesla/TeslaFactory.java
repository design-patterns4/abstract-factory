package ru.design.patterns.tesla;

import static java.util.Arrays.asList;

import lombok.Setter;
import ru.design.patterns.cartype.Car;
import ru.design.patterns.cartype.Hatchback;
import ru.design.patterns.cartype.Minivan;
import ru.design.patterns.cartype.Sedan;
import ru.design.patterns.enums.Color;
import ru.design.patterns.factory.CarFactory;

import java.util.ArrayList;
import java.util.List;

/**
 * TeslaFactory.
 *
 * @author Tatyana_Dolnikova
 */
@Setter
public class TeslaFactory implements CarFactory {

    private final Color defaultColor = Color.BLACK;
    private Integer sedanSpeed = 250;
    private Integer hatchbackSpeed = 400;
    private Integer minivanSpeed = 200;
    private Double price = 25000.0;
    private Double rate = 1.5;

    @Override
    public Sedan createSedan() {
        return new TeslaSedan(asList(defaultColor), sedanSpeed);
    }

    @Override
    public Hatchback createHatchback() {
        return new TeslaHatchback(asList(defaultColor), hatchbackSpeed);
    }

    @Override
    public Minivan createMinivan() {
        return new TeslaMinivan(asList(defaultColor), minivanSpeed);
    }

    /**
     * Покрасить авто в несколько цветов.
     */
    @Override
    public Car paintCar(Car car, Color color) {
        List<Color> colors = car.getColors();
        if (colors == null) {
            colors = new ArrayList<>();
        }
        if (!colors.contains(color)) {
            colors.add(color);
        }
        car.setColors(colors);
        return car;
    }

    /**
     * Получить полную стоимость авто на основании цены и коэффициента за скорость.
     */
    @Override
    public Double getPrice(Car car) {
        return price + car.getSpeed() * rate;
    }

}
