package ru.design.patterns.tesla;

import ru.design.patterns.cartype.Sedan;
import ru.design.patterns.enums.Color;

import java.util.List;

/**
 * TeslaSedan.
 *
 * @author Tatyana_Dolnikova
 */
public class TeslaSedan extends Sedan {

    public TeslaSedan(List<Color> colors, Integer speed) {
        super(colors, speed);
    }
}
