package ru.design.patterns.tesla;

import ru.design.patterns.cartype.Hatchback;
import ru.design.patterns.enums.Color;

import java.util.List;

/**
 * TeslaHatchback.
 *
 * @author Tatyana_Dolnikova
 */
public class TeslaHatchback extends Hatchback {

    public TeslaHatchback(List<Color> colors, Integer speed) {
        super(colors, speed);
    }
}
