package ru.design.patterns.tesla;

import ru.design.patterns.cartype.Minivan;
import ru.design.patterns.enums.Color;

import java.util.List;

/**
 * TeslaMinivan.
 *
 * @author Tatyana_Dolnikova
 */
public class TeslaMinivan extends Minivan {

    public TeslaMinivan(List<Color> colors, Integer speed) {
        super(colors, speed);
    }
}
