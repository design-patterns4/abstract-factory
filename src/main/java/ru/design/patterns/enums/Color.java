package ru.design.patterns.enums;

/**
 * Color.
 *
 * @author Tatyana_Dolnikova
 */
public enum Color {
    RED,
    BLACK,
    WHITE,
    GREEN,
    YELLOW,
    BLUE
}
