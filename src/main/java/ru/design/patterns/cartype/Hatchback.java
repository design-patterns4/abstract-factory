package ru.design.patterns.cartype;

import ru.design.patterns.enums.Color;

import java.util.List;

/**
 * Hatchback.
 *
 * @author Tatyana_Dolnikova
 */

public abstract class Hatchback extends Car {

    public Hatchback(List<Color> colors, Integer speed) {
        super(colors, speed);
    }
}
