package ru.design.patterns.cartype;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.Setter;
import ru.design.patterns.enums.Color;

import java.util.List;

/**
 * Car.
 *
 * @author Tatyana_Dolnikova
 */
@Getter
@Setter
@AllArgsConstructor
public abstract class Car {
    List<Color> colors;
    Integer speed;
}
