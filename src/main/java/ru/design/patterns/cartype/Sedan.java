package ru.design.patterns.cartype;

import ru.design.patterns.enums.Color;

import java.util.List;

/**
 * Email.
 *
 * @author Tatyana_Dolnikova
 */
public abstract class Sedan extends Car {

    public Sedan(List<Color> colors, Integer speed) {
        super(colors, speed);
    }
}
